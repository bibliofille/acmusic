<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_acmusic');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9:6()# ) 2lKv7rZ9,hwb=)Jlk6js)||qU%}Oemdpzp_$<%4hgg)VNa6}%_RA%t0');
define('SECURE_AUTH_KEY',  'm[([iP00>V5c@VOv91OrHqn{_j?%}&N-%[WrH~5mwEmmP&%>r|V9Ob*$Y;J5ez4(');
define('LOGGED_IN_KEY',    '}C L$X7~e-~aj_ +0r`q-;$U!+=;A!+@PevY!WaC]2p00LHd5g bLUFKxm!V 2OY');
define('NONCE_KEY',        'ih {%`YC$qW%JpizR#vX^ xs*Bvz{RSc1ID1 RhoR;!yRj,Pw1>HAo):<w$N)NYb');
define('AUTH_SALT',        'bTmnH4N&|G7zD.*qkQ:f[5MR,Fe1UHs#|H&)zewB3k*a.Sz=7JwtKHm|58w{L^a=');
define('SECURE_AUTH_SALT', '8gn)XLbT!#25$2!yNE0g#G|}CS/B-`:x8.Z/rOVkaCA|;L=v9J)OWThI%,2)P}/t');
define('LOGGED_IN_SALT',   ',@S:/KukD/2fDNJ)=;EZ)W4 wB`] ;hUtuEMsD9jU9R2r#|!./k4=G^If|>tGsN]');
define('NONCE_SALT',       'C;&OG P/nhmo23dHR+]4n7.a$p95ym08{7MPxKXO3eh$TJ:iGIiLVvA+VuAI,~ee');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
