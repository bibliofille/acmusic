<?php
/*
 * Members Custom Post Type Archive
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<div class="page-heading-block"><h1 class="archive-title">Composers</h1>

						<p class="subheading-content">One of our goals is to be the biggest commissioning source for composers and ensembles around the world. Here are some composers we've worked with and a sample of their projects.</p>

						</div>	

						<div class="commissions-filter"><?php echo do_shortcode('[searchandfilter id="6130"]'); ?></div>

						<div class="commissions-container">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


							<div class="commission-item">


									<?php the_post_thumbnail('full'); ?>

									<h3><?php the_title(); ?></h3>

									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="read-more-button">Read More</a>

							
						</div>

							<?php endwhile; ?>



							
									

							<?php endif; ?>


									

						</div>

						<?php bones_page_navi(); ?>

						</main>

				</div>

			</div>

<?php get_footer(); ?>
