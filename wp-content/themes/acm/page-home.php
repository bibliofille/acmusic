<?php
/*
 Template Name: Home Page 
 *
*/
?>

<?php get_header('home'); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


							<div class="intro-block">

								<div class="homepage-video">
									<?php the_field('homepage_video'); ?>
								</div>

								<div class="intro-block-content">
								<p><?php the_field('intro_text'); ?></p>

								<a href="<?php the_field('intro_link'); ?>" class="intro-button">More About Us</a>
							</div>

							</div>


							<?php get_template_part( 'home-news-feed' ); ?>


							

						</main>


				

			</div>

			<div class="home-productions-block">

			<?php get_template_part( 'home-productions-feed' ); ?>

		</div>

			<div class="home-composer-block">

				<?php get_template_part( 'home-featured-composers' ); ?>

			</div>

			

	</div>

</div>


<?php get_footer(); ?>
