			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>
					<div class="footer-columns">
<div class="footer-column-1">
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="https://acmusic.us2.list-manage.com/subscribe/post?u=02d8496908b9d46cd559d7c01&amp;id=e9b49cd5b8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<h2>Get updates in your inbox</h2>
<div class="mc-field-group">
	<label for="mce-EMAIL">Your email address:</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_02d8496908b9d46cd559d7c01_e9b49cd5b8" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Sign up" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->

<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></p>
</div>

<div class="footer-column-2">
	<a href="http://localhost:8888/acmusic/category/listen-watch/" class="footer-button">Listen/Watch</a>
	<ul class="footer-social">
		<li><a href="https://www.facebook.com/Access-Contemporary-Music-225511967461822/" target="_blank">
			<span class="fa-stack fa-lg">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-facebook fa-stack-1x"></i>
</span></a></li>
		<li><a href="https://www.instagram.com/acmchicago/" target="_blank">
			<span class="fa-stack fa-lg">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-instagram fa-stack-1x"></i>
</span></a></li>
<li><a href="https://vimeo.com/acmusic/videos" target="_blank">
			<span class="fa-stack fa-lg">
  <i class="fa fa-circle-thin fa-stack-2x"></i>
  <i class="fa fa-vimeo fa-stack-1x"></i>
</span></a></li>
	</ul>
</div>

<div class="footer-column-3">
	<img src="http://localhost:8888/acmusic/wp-content/uploads/2017/03/ACM-logo_whoknows.png">
	<p>1758 W. Wilson St., Chicago, IL<br>
+1 (773) 334-3650</p>
<a href="mailto:info@acmusic.org" class="footer-button">Email Us</a>
	</div>
	</div>
				

				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
