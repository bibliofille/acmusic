

<div class="home-featured-composers wrap">

	<div class="page-heading-block">

	<h2>Meet our composers</h2>

	<p class="subheading-content"><?php the_field('composer_subheading'); ?></p>

</div>

	<div class="featured-composers">

		<div class="featured-composers-subheader">

								<div class="comoposer-video">
									<?php the_field('composer_video'); ?>
								</div>

								<div class="featured-composer-content">
									<?php 

										$image = get_field('featured_composer_image');
										$size = 'full'; // (thumbnail, medium, large, full or custom size)

										if( $image ) {

											echo wp_get_attachment_image( $image, $size );

										}

									?>

									<h4><?php the_field('featured_composer_heading'); ?></h4>

									<p><?php the_field('featured_composer_subheading'); ?></p>

									<a href="<?php the_field('page_link'); ?>" class="read-more-button">Read More</a>

								</div>	

							</div>

						</div>

								

<div class="home-commissions-container">

									    <?php if( have_rows('commissions') ): ?>
 
      
            <?php while ( have_rows('commissions') ) : the_row(); ?>   
 
 <?php // set up post object
        $post_object = get_sub_field('commissions');
        if( $post_object ) :
        $post = $post_object;
        setup_postdata($post);
        ?>

<div class="commission-item">
 		
        <?php the_post_thumbnail('full'); ?>
  		<h4><?php the_title(); ?></h4>
        <a href="<?php the_permalink(); ?>" class="read-more-button">Read More</a>

        </div>
 
                        <?php wp_reset_postdata(); ?>
 
                    <?php endif; ?>
              
 
            <?php endwhile; ?>
 
           
 
    <?php endif; ?>



</div>


								








</div>