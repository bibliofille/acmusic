<?php
/*
 Template Name: Staff Page 
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


								<div class="entry-content cf staff" itemprop="articleBody">

									<h1>Staff</h1>

								
	

									<?php if( have_rows('staff') ): ?>


	<?php while( have_rows('staff') ): the_row(); 

		// vars
		$name = get_sub_field('name');
		$title = get_sub_field('title');
		$bio = get_sub_field('bio');

		?>
			<div class="staff-row">

				<?php 

$image = get_sub_field('image');
$size = 'large'; // (thumbnail, medium, large, full or custom size)

if( $image ) {

	echo wp_get_attachment_image( $image, $size );

}

?>

		    <div class="staff-content">
		    <h5><?php echo $name; ?></h5>
		    <p><?php echo $title; ?></p>
		    <p><?php echo $bio; ?></p>

		</div>

</div>

	<?php endwhile; ?>




<?php endif; ?>


							
									
								</div>

								<div class="entry-content cf board" itemprop="articleBody">

									<h1>Board of Directors</h1>

									<div class="board-container">

									<?php if( have_rows('board') ): ?>

	

	<?php while( have_rows('board') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$name = get_sub_field('name');
		$title = get_sub_field('title');

		?>

		<div class="board-item">

				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />

		   
		    <h5><?php echo $name; ?></h5>
		    <p><?php echo $title; ?></p>

	</div>

	<?php endwhile; ?>



<?php endif; ?>
</div>
									
								</div>


							

						

							

						</main>


				</div>

			</div>


<?php get_footer(); ?>
