<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div id="inner-content" class="wrap cf">

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>


			

		<div class="teacher-content">

		<div class="teacher-heading">	
		<h1><?php the_field('teacher_section_heading'); ?></h1>
		<p><?php the_field('teacher_section_subheading'); ?></p>
	</div>

	<div class="teacher-container">

		<?php if( have_rows('teacher_list') ): ?>



	<?php while( have_rows('teacher_list') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$name = get_sub_field('name');
		$subject = get_sub_field('subject');
		$location = get_sub_field('location');
		$link = get_sub_field('read_more_link');

		?>
<div class="teacher-item">

				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />

		    <h5><?php echo $name; ?></h5>
		    <p><?php echo $subject; ?>&nbsp;|&nbsp;<?php echo $location; ?></p>
		    <a href="<?php echo $name; ?>" class="teacher-link">Read More</a>


</div>


		

	<?php endwhile; ?>


<?php endif; ?>


	</div>



		</div>	

		</div>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
