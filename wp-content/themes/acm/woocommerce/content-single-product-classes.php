<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Hook Woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<?php
			/**
			 * Hook: Woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>
	</div>

	<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

	<div class="teacher-content">

		<div class="teacher-heading">	
		<h1><?php the_field('teacher_section_heading'); ?></h1>
		<p><?php the_field('teacher_section_subheading'); ?></p>
	</div>

	<div class="teacher-container">

		<?php if( have_rows('teacher_list') ): ?>



	<?php while( have_rows('teacher_list') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$name = get_sub_field('name');
		$subject = get_sub_field('subject');
		$location = get_sub_field('location');
		$link = get_sub_field('read_more_link');

		?>
<div class="teacher-item">

				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />

		    <h5><?php echo $name; ?></h5>
		    <p><?php echo $subject; ?>&nbsp;|&nbsp;<?php echo $location; ?></p>
		    <a href="<?php echo $link; ?>" class="teacher-link">Read More</a>


</div>


		

	<?php endwhile; ?>


<?php endif; ?>


	</div>



		</div>	

<?php do_action( 'woocommerce_after_single_product' ); ?>
