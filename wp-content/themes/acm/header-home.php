<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
			<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,400,400i,700,700i" rel="stylesheet">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div id="container">

			<div class="cart-donate-container">
					<a class="cart-customlocation cart-header" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><span>CART:&nbsp;</span><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></a>
					<a class="header-donate-button" href="<?php echo get_site_url(); ?>/donations">Donate</a>
				</div>

				<?php if( get_field('banner_image') ): ?>

			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader" style="background-image: url(<?php the_field('banner_image'); ?>);">

				<?php else: ?>

				<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader" style="background-image: url('<?php echo get_home_url(); ?>/wp-content/uploads/2020/02/Performances-long-1024x183.jpg'); ?>);">	

			<?php endif; ?> 

				<div class="homepage-logo">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2017/03/ACM-logo_whoknows.png">
				</div>
<div class="header-unit">
    <div id="video-container">
        <video muted autoplay loop class="fillWidth">
            <source src="<?php echo get_home_url(); ?>/wp-content/uploads/2015/12/web-video-HD.mp4" type="video/mp4"/>
       </video>

       <script type="text/javascript">

    $(document).ready(function() {

        var breakpoint = 1030;

        loadVideo();

       

        // Play video when page resizes
        $(window).resize(function() {
            loadVideo();
        });

       

            // Add source tags if not already present
            if ($(document).width() > breakpoint) {
                if (document.querySelectorAll("#herovideo > source").length < 1) {
                    var source1 = document.createElement('source');
                    var source2 = document.createElement('source');
                    var source3 = document.createElement('source');

                    source1.setAttribute('src', '/tablet.mp4');
                    source1.setAttribute('type', 'video/mp4');

                    source2.setAttribute('src', '/tablet.webm');
                    source2.setAttribute('type', 'video/webm');

                    source3.setAttribute('src', '/tablet.ogv');
                    source3.setAttribute('type', 'video/ogg');

                    video.appendChild(source1);
                    video.appendChild(source2);
                    video.appendChild(source3);
                }

                // Play the video
                $('.fillWidth')[0].play();
            }
        }
    });
</script>
    </div><!-- end video-container -->
</div><!-- end .header-unit -->

				</header>

				<div id="nav-header">

				<div id="inner-header">

					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<div id="logo" class="h1" itemscope itemtype="http://schema.org/Organization"><a href="<?php echo home_url(); ?>" rel="nofollow"><img src="http://localhost:8888/acmusic/wp-content/uploads/2016/01/access_contemporary_music-2.jpg"></a></div>

					<?php // if you'd like to use the site description you can un-comment it below ?>
					<?php // bloginfo('description'); ?>


					<nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
						<?php wp_nav_menu(array(
    					         'container' => false,                           // remove nav container
    					         'container_class' => 'menu cf',                 // class of container (should you choose to use it)
    					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
    					         'menu_class' => 'nav top-nav cf',               // adding custom nav class
    					         'theme_location' => 'main-nav',                 // where it's located in the theme
    					         'before' => '',                                 // before the menu
        			               'after' => '',                                  // after the menu
        			               'link_before' => '',                            // before each link
        			               'link_after' => '',                             // after each link
        			               'depth' => 0,                                   // limit the depth of the nav
    					         'fallback_cb' => ''                             // fallback function (if there is one)
						)); ?>

					</nav>

						<div id="mobile-menu-container"></div>


					<div class="search-dropdown">
            <form method="get" id="searchform" action="<?php echo site_url(); ?>">
	<input type="text" class="field" name="s" id="s" placeholder="What are you looking for?">
	<button type="submit" class="submit" name="submit" id="searchsubmit"><i class="fa fa-search fa-lg"></i></button>
</form>        
</div>

				</div>


			</div>

			
