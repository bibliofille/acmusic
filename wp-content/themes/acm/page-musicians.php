<?php
/*
 Template Name: Musicians page 
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="page-title"><?php the_title(); ?></h1>


								</header>

								
									<?php the_content();?>



							<?php 

				// check for rows (parent repeater)
				if( have_rows('musician_section') ): ?>
					<?php 

					// loop through rows (parent repeater)
					while( have_rows('musician_section') ): the_row(); ?>
						
							<h2 class="section-title"><?php the_sub_field('section_title'); ?></h2>
							<div class="musician-container">
							<?php 

							// check for rows (sub repeater)
							if( have_rows('musician_info') ): ?>
								
								<?php 

								// loop through rows (sub repeater)
								while( have_rows('musician_info') ): the_row();	

								// vars
								$image = get_sub_field('image');
								$name = get_sub_field('name');
								$instrument = get_sub_field('instrument');
								$bio = get_sub_field('bio');

								?>

								<div class="musician-item">

								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
								<h4><?php echo $name; ?></h4>
								<h5><?php echo $instrument; ?></h5>
								<p><?php echo $bio; ?></p>
									
								<?php endwhile; ?>
								
							</div>

							<?php endif; //if( get_sub_field('musician_info') ): ?>
						</div>	

					<?php endwhile; // while( has_sub_field('musician_block') ): ?>
					
				<?php endif; // if( get_field('musician_block') ): ?>











								


							

							</article>

								<?php endwhile; ?>

							<?php endif; ?>

						</main>


				</div>

			</div>


<?php get_footer(); ?>
