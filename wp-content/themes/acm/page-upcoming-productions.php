<?php
/*
 * Template name: Upcoming Productions
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<div class="page-heading-block">
							<h1>Upcoming Shows</h1>

						<div class="subheading-content"><?php the_field('header_paragraph'); ?></div>
					</div>
							
						<?php 
						//Set server timezone to central
						date_default_timezone_set('America/Chicago'); 
						//Today's date
						$date_1 = date('Y-m-d H:i:s', strtotime("now")); 
						//Future date - the arg will look between today's date and this future date to see if the post fall within the 2 dates.
						$date_2 = date('Y-m-d H:i:s', strtotime("+12 months"));
						?>
						


						<?php //arg to determine if the post is an upcoming event.
						$upcoming_args = array(
						'post_type'		=> 'productions',
						'posts_per_page'	=> 10,
						'meta_key' => 'event_date',
						'meta_compare' => 'BETWEEN',
						'meta_type' => 'datetime',
						'meta_value' => array($date_1, $date_2),
						'orderby' => 'event_date',
						'order' => 'ASC'
							); 
						?>


						<?php 
						// the upcoming events query
						$upcoming_query = new WP_Query( $upcoming_args ); 
						?>


<div class="upcoming-productions-container">

<?php if ( $upcoming_query->have_posts() ) : ?>					
  <!-- the loop -->
  <?php while ( $upcoming_query->have_posts() ) : $upcoming_query->the_post(); ?>
  

							<div class="upcoming-production">

									<?php if ( has_post_thumbnail() ) : ?>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail('production-archive'); ?>
    </a><br>
    <h3><?php the_title(); ?></h3>
   <?php 

// get raw date
$date = get_field('event_date', false, false);


// make date object
$date = new DateTime($date);

?>
<div class="event-details">
<p><?php echo $date->format('l, F j, Y'); ?></p>



     <p><?php the_field('event_location'); ?></p>
      <p><?php the_field('ticket_price'); ?></p>
  </div>
    <a href="<?php the_permalink(); ?>" class="read-more-button">Read More</a>
<?php endif; ?>
</div>

							

							<?php endwhile; ?>

							

							

						

									<?php wp_reset_postdata(); ?>


								

									

							<?php endif; ?>

						</div>

						<?php bones_page_navi(); ?>


						</main>


				</div>

			</div>

<?php get_footer(); ?>
