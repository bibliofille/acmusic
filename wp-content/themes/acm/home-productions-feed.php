

<div class="home-productions-feed wrap">

  <div class="productions">

    <div class="page-heading-block">

	<h1>Current Productions</h1>

	<p class="subheading-content"><?php the_field('productions_subheading'); ?></p>

</div> 

<div class="productions-container">

 <?php if( have_rows('productions_feed') ): ?>
 
      
            <?php while ( have_rows('productions_feed') ) : the_row(); ?>   
 
 <?php // set up post object
        $post_object = get_sub_field('production');
        if( $post_object ) :
        $post = $post_object;
        setup_postdata($post);
        ?>

<div class="home-productions-item">
    
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a><br>
      <h4><?php the_title(); ?></h4>
        <a href="<?php the_permalink(); ?>" class="read-more-button">Read More</a>

        </div>
 
                        <?php wp_reset_postdata(); ?>
 
                    <?php endif; ?>
              
 
            <?php endwhile; ?>
 
           <?php endif; ?>
 

</div>



</div>


</div>