<?php
/*
 Template Name: Contact Page 
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

			

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>


								

								<section class="entry-content cf contact-page-container" itemprop="articleBody">

									<div class="address-box">
										<h5>Say hello!</h5>
										<p>Access Contemporary Music<br>
										1758 W. Wilson St.<br>
										Chicago, IL 6064 </p>
										<p><a href="mailto:info@acmusic.org">info@acmusic.org</a></p>

								</div>

									
								<div class="contact-form"><?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?></div>

								</section> <?php // end article section ?>

								

						</main>

				

				</div>

			</div>

<?php get_footer(); ?>
