<?php
/*
 * Template name: Past Productions
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<h1 class="archive-title h2">Past Productions</h1>

					
							<?php 
						//Set server timezone to central
						date_default_timezone_set('America/Chicago'); 
						//Today's date
						$today = date('Y-m-d H:i:s'); 
						?>


						<?php //arg to determine if the post is a past event.
						$args = array(
						'post_type'		=> 'productions',
						'posts_per_page'	=> 10,
						'order' => 'DESC',
						'meta_query' => array(
        array(
            'key' => 'event_date',
            'value' => $today,
            'compare' => '<=',
        ),
        )

							); 
						?>


						<?php 
						// the past events query
						$query = new WP_Query( $args ); 
						?>


<?php if ( $query->have_posts() ) : ?>					
  <!-- the loop -->
  <?php while ( $query->have_posts() ) : $query->the_post(); ?>

  

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

								<header class="article-header">

									<h3 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
									

								</header>

								<section class="entry-content cf">

									<?php if ( has_post_thumbnail() ) : ?>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail('bones-thumb-300'); ?>
    </a>
<?php endif; ?>

								</section>

								<footer class="article-footer">

								</footer>

							</article>

							<?php endwhile; ?>

						
						

									<?php wp_reset_postdata(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the custom posty type archive template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

									<?php bones_page_navi(); ?>

							<?php endif; ?>


						</main>


				</div>

			</div>

<?php get_footer(); ?>
