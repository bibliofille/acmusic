<?php
/*
 Template Name: Donate Page 
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


								<div class="entry-content cf donations" itemprop="articleBody">

									
									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>

									<div class="donations-container">

										<div class="donations-item">
											<?php the_field('donate_text'); ?>
										</div>

					<div class="donations-item">

					<a href="<?php echo home_url(); ?>/product/donation/" class="donate-page-button">Donate Online</a>

					<div class="donate-address">
						<h5>Donate by mail</h5>
						<p>Access Contemporary Music<br>
						1758 W. Wilson St.<br>
						Chicago, IL 60640</p>

					</div>

				</div>

				</div>
							
									
								</div>

								<div class="entry-content cf current-donors" itemprop="articleBody">

									<h1>Current Donors</h1>

									<section class="current-donor-content">

						<?php

// check if the flexible content field has rows of data
if( have_rows('current_donors') ): ?>

<?php while( have_rows('current_donors') ): the_row(); 

		// vars
		$range = get_sub_field('donor_range');
		$names = get_sub_field('donor_names');
		


        if( get_row_layout() == 'donor_block' ):

        	echo '<h5>'; the_sub_field('donor_range'); echo '</h5>';

        echo '<p>'; the_sub_field('donor_names');  echo '</p>';

        endif;

    endwhile;


endif;

?>

</section>
									
								</div>


							

							

							

						</main>


				</div>

			</div>


<?php get_footer(); ?>
