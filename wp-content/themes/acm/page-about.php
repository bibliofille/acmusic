<?php
/*
 Template Name: About Page 
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">


								<div class="page-heading-block">

									<h1 class="page-title">Mission/Vision</h1>

									<div class="subheading-content">
										<?php the_field('subheading_content'); ?>

									</div>

								</div>


								</header>


								<section class="entry-content cf about-top" itemprop="articleBody">

									<h2>Mission</h2>

									<p class="large-paragraph"><?php the_field('mission'); ?></p>

									<h2>Vision</h2>

									<p class="large-paragraph"><?php the_field('vision'); ?></p>

									<h2>Strategy</h2>

									<p><?php the_field('strategy'); ?></p>

									<a href="<?php echo home_url(); ?>/productions" class="about-button">More about our productions</a>


								</section>

								<section class="entry-content about-video cf" itemprop="articleBody">

									<div class="embed-container"><?php the_field('video'); ?></div>

								</section>

								<section class="entry-content cf about-bottom" itemprop="articleBody">

									<h1>History</h1>

									<p><?php the_field('history'); ?></p>

								</section>


							</article>

							



						</main>


				</div>

			</div>


<?php get_footer(); ?>
