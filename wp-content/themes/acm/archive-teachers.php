<?php
/*
 * TEACHERS CUSTOM POST TYPE ARCHIVE TEMPLATE
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<div class="page-heading-block"><h1 class="archive-title">Teachers</h1>

						</div>	


						<div class="teachers-container">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


							<div class="teachers-item">


									<?php the_post_thumbnail('full'); ?>

									<h3><?php the_title(); ?></h3>

									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="read-more-button">Read More</a>

							
						</div>

							<?php endwhile; ?>



							
									

							<?php endif; ?>


									

						</div>

						<?php bones_page_navi(); ?>

						</main>

					

				</div>

			</div>

<?php get_footer(); ?>
