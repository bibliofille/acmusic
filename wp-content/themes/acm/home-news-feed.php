

<div class="home-news-feed">

	<h1>News Feed</h1>

<div class="news-feed-container">

<?php 
   // the query
   $the_query = new WP_Query( array(
     'category_name' => 'uncategorized',
      'posts_per_page' => 3,
   )); 
?>

<?php if ( $the_query->have_posts() ) : ?>
  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="home-news-item">
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'large' ); ?></a><br>
  <h5><?php the_title(); ?></h5>
  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="read-more-button">Read More</a>

</div>

    

  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php __('No News'); ?></p>
<?php endif; ?>

</div>

<div class="news-button-wrapper">
  <a href="<?php echo get_home_url(); ?>/news" class="all-news-button">See All News</a>
</div>




</div>