<?php
/*
 * Productions Custom Post Type Archive Template 
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

						<h1 class="archive-title">Past Productions</h1>

						<div class="productions-filter"><?php echo do_shortcode('[searchandfilter id="6128"]'); ?></div>

						<div class="past-productions-container">


						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



							<div class="past-production">


								

								    	<?php if ( has_post_thumbnail() ) : ?>
								        <?php the_post_thumbnail('full'); ?>
								        <?php endif; ?>
									

							<h3><?php the_title(); ?></h3>

							<a href="<?php the_permalink(); ?>" class="read-more-button">Read More</a>

							


						</div>



							<?php endwhile; ?>


						

							<?php endif; ?>

							

						</div>

						<?php bones_page_navi(); ?>



						</main>


				</div>

			</div>

<?php get_footer(); ?>
